const sinon = require('sinon');

const { commentsFactory } = require('../src/factory-module');
const helperModule = require('../src/helper-module');
const createComments = require('../src/common-module');


describe('Modules', () => {

    describe('Common Module', () => {
        let loggerSpy;
        let commentValidatorSpy;
        let addCommentSpy;
        let removeCommentSpy;
        let commentsObject;

        beforeEach(() => {
            loggerSpy = sinon.spy(helperModule, 'logger');
            commentValidatorSpy = sinon.spy(helperModule, 'commentValidator');
            addCommentSpy = sinon.spy(commentsFactory, 'addComment');
            removeCommentSpy = sinon.spy(commentsFactory, 'removeComment');

            commentsObject = createComments();
        })
        afterEach(() => {
            loggerSpy.restore();
            commentValidatorSpy.restore();
            addCommentSpy.restore();
            removeCommentSpy.restore();
        });

        it('should return correct results after manipulations with comments and log number of comments', () => {
            expect(commentsFactory.addComment.callCount).to.equal(3)
            expect(commentsFactory.addComment.calledWith(sinon.match.instanceOf(helperModule.CommentModel))).to.be.true;

            expect(commentsFactory.removeComment.callCount).to.equal(1)
            expect(commentsFactory.removeComment.calledWith('1')).to.be.true;

            expect(helperModule.logger.called).to.be.true;
            commentsFactory.getComments().forEach(comment => {
                expect(helperModule.commentValidator.calledWith(comment)).to.be.true;
            })

            expect(commentsObject.comments.length).to.equal(2);
            expect(commentsObject.comments[0].id).to.equal('2');
            expect(commentsObject.comments[1].id).to.equal('3');
            expect(commentsObject.areValid).to.be.true;
        });

    });

});
