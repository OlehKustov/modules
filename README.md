## Modules

Use **CommonJS** syntax for this task. Your are given with three modules `common-module.js`, `factory-module.js` and `helper-module.js`.
Each module serves its own purpose and delivers certain functionality:

`factory-module.js` is going to deliver CommentsFactory class with the following methods:
 - addComment - adds comment to the list;
 - removeComment - removes comment from the list;
 - getComment - returns comment based on provided id;
 - getComments - returns all comments;
 
Export should contain two properties: `commentsFactory` with class instance and `commentsFactoryClass` with class itself.

`helper-module.js` is going to provide us with useful functionality, namely:
 - `logger(message, result)` - logs the output to the console where message is our custom input and result is the result
  we want to log:
 ```
commentsFactory = new CommentsFactory();
 logger('Number of comments' commentsFactory.getComments())'
// Output: 'Number of comments: []'
```
 - `commentValidator(comment)` - checks validity of the comment. Comment shall be considered invalid if there is no text 
 or author provided as well as if comment is longer than 100 characters. Otherwise, comment is considered as valid.
 - `CommentModel` class - represents the model of our comment which possess the following properties: `id`, `text`, 
 `author`. The `id` property is just a value of type `Sting` starting from '1', '2', etc... (it should be generated later 
 on the fly).

Export should contain three properties: `logger` with logger function, `commentValidator` with commentValidator function
and `CommentModel` with the class (not instance). 

Finally, the `common-module.js` is the module where the action takes place:
 
 - Add three valid comments with the help of CommentModel (write any text and author name you prefer);
 - Remove first element from the list;
 - Log number of comments - two is expected (write any message you prefer);

Last but not least, wrap your code around createComments() function which returns an object of such a model:
```
{
    comments: Array<CommentModel>, // Array of all comments
    areCommentsValid: boolean, // Validity of all comments, true is expacted
}
```

`common-module.js` should export only createComments function (not its result).

Make sure to import functionality from the helper and factory modules as modules! (Do not
destructure modules). In `common-module` you should use your functionality as follows:

```
factoryModule.commentsFactory.addComment(args)
helperModule.logger(args)
new helperModule.CommentModel(args)
```
 
**NB!** Use the names of functions/methods/classes described above to avoid unexpected errors.
